<?php
/**
 * @owner xXAlphaManXx
 * @editedOn 26/10/16 ::: 17:22
 * @package Smart Patch
 * @file PageHandler.php
 *
 * Licensed to Smart Hacks Inc.
 * @link https://smarthacksinc.com
 *
 * @repo https://github.com/smarthacks/
 *
 */
if (!defined(ACCESS)) {
    die("FILE IS NOT ALLOWED TO BE ACCESSED DIRECTLY");
}