# Smart Patch

An awesome, user friendly WEB based patch managment system that can simplify the pain of upload patches to a server for using it in your game.

# Install

As I've made this script only for simple and easy set up, you can install it easily.

* First, upload the contents to your server.
* Go to PhpMyAdmin and create a database with a user.
* Make sure that the user has all the previlages.
* Now open the 
``` /php/config/Config.conf.php ``` and then edit as per your feelings :)
* Awesome, now open up the website and create an account
* Back on PhpMyAdmin, go to your database and you will notice your account. In that, `user_role` column will be at `0`, edit it and set it to `1`
* Now you are configured to have your account being recognised as an ADMIN. Many user roles to be added in the new upcomming versions.

# Theme

We use INSPINIA admin panel theme with our project. For the sake of the author's hard work, i should buy a extended license and i am in need to pay $1000. So pleae donate as much as possible to me so i can keep delivering you the awesome project no matter what.

# License

I was a fan of GPL, but due to CC'S high popularity, i am using CC.

# Developer

xXAlphaManXx - Hacker, blogger and a coder.

***Email*** - alpha@smarthacksinc.com

***WEB*** - [Smart Hacks](https://smarthacksinc.com/)

***SKYPE*** - karansanjeev

***DISCORD*** - xXAlphaManXx#6439

# DONATE 

Your each dollar is like a diamond. Each dollar helps me a lot than you think. So, if you feel like you can give your money (little) to me, it would be awesome.

<a href="https://ko-fi.com/A422ACK"><img src="https://az743702.vo.msecnd.net/cdn/kofi1.png?v=b" alt="Buy Me a Coffee at ko-fi.com" style="max-width:100%;" border="0" height="32"></a>
